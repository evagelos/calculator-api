import re

from calculator.simple import SimpleCalculator


class Calculation:

    def __init__(self, input_):
        self.input = input_
        self.validate()

    def validate(self):
        """Raise any error if input is invalid."""
        # FIXME add support for floats
        # TODO improve pattern:
        # - pair of (+/-, Number) at least 2 times
        # - chars * and / should be between 2 numbers
        # - cant end with operand or it can be ignored
        if invalid_chars := re.findall(r"[^\-\+\*\/0-9\s]", self.input):
            raise ValueError(f'Invalid characters: {invalid_chars}')

    def add_space_between_operands(self):
        """Add space between operands."""
        string = self.input.replace(' ', '')
        for operand in ('+', '-', '*', '/'):
            string = string.replace(operand, f' {operand} ')
        return string

    def calculate(self):
        """Return the result of the calculation."""
        string = self.add_space_between_operands()
        simple_calculator = SimpleCalculator()
        simple_calculator.run(string)
        return simple_calculator.lcd
