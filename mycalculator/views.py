from .calculator import Calculation


def calculate_view(request):
    response = {'message': '', 'result': 0}

    if not request.query_string:
        response['message'] = (
            "Welcome, request a calculation to be made using the query string, for example: "
            f"{request.application_url}?1+2 "
        )
        return response

    try:
        calculation = Calculation(request.query_string)
    except ValueError as err:
        response['message'] = str(err)
    else:
        response['result'] = calculation.calculate()

    return response
