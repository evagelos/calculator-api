import pytest

from mycalculator.calculator import Calculation


@pytest.mark.parametrize("input_", [
    "a",
    "1a",
    "1+a",
])
def test_invalid_calculations(input_):
    with pytest.raises(ValueError) as err:
        Calculation(input_)


@pytest.mark.parametrize("input_, expected", [
    ("1+2", 3),
    ("3*2", 6),
    ("2/4", 0.5),
    ("2-1", 1),
    ("1-2", -1),
])
def test_valid_calculations(input_, expected):
    c = Calculation(input_)
    assert c.calculate() == expected


@pytest.mark.parametrize("input_, expected", [
    ("1+2", "1 + 2"),
    ("1 + 2", "1 + 2"),
    ("1 +2", "1 + 2"),
    ("1+ 2", "1 + 2"),
])
def test_adding_spaces_between_operands(input_, expected):
    c = Calculation(input_)
    assert c.add_space_between_operands() == expected
