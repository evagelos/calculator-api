from pyramid import testing
from webtest import TestApp
import pytest

from app import make_app


@pytest.fixture(scope="module")
def app():
    """Create once an application for all tests."""
    app = make_app()
    testapp = TestApp(app)
    yield testapp


def test_calculate_view(app):
    res = app.get('/')
    assert res.json['message'].startswith('Welcome') and res.json['result'] == 0


def test_calculate_view_valid_input(app):
    res = app.get('/?1+2')
    assert res.json['result'] == 3.0


def test_calculate_view_invalid_input(app):
    res = app.get('/?1&2')
    assert res.json['message'].startswith('Invalid characters')
