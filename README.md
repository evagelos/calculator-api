# Calculator - Simple API to make arithmetic calculations

[![coverage report](https://gitlab.com/evagelos/calculator-api/badges/master/coverage.svg)](https://gitlab.com/evagelos/calculator-api/-/commits/master)
<br/><br/>

## How to use
Uses 1 endpoint the root /

Request a calculation by appending it as a query string, example: `http://localhost:6543?1+2`
<br/><br/>
## Install and Run manually
```sh
$ python -m pip install -r requirements.txt
$ python app.py
```
### Running the tests
```sh
$ python -m pytest
```

## Install and Run with Docker
```
$ docker build -t calculator .
$ docker run -p 6543:6543 -it calculator
```
<br/><br/>
## API Documentation
While running the application head over to http://localhost:6543/api/v1/
<br/><br/>
## TODO
- [ ] support floats
- [ ] support parenthesis
- [ ] improve pattern match
