from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response

from mycalculator.views import calculate_view


def make_app():
    with Configurator() as config:
        config.include('pyramid_openapi3')
        config.pyramid_openapi3_spec('openapi.yaml', route='/api/v1/openapi.yaml')
        config.pyramid_openapi3_add_explorer(route='/api/v1/')

        config.add_route('calculate_view', '/')
        config.add_view(
                calculate_view,
                route_name='calculate_view',
                renderer='json',
                request_method='GET',
                openapi=True
        )
        return config.make_wsgi_app()


if __name__ == '__main__':
    app = make_app()
    server = make_server('0.0.0.0', 6543, app)
    server.serve_forever()
